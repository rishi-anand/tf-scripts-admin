locals {
  gitlab_project_ids = {
    for k, v in gitlab_project.this :
    v.name => v.id
  }
}

resource "gitlab_project" "this" {
  for_each = local.projects

  name                   = each.value.name
  description            = each.value.description
  visibility_level       = "public" # or 'private'
  pipelines_enabled      = true
  shared_runners_enabled = true # shared runners means runners from different project can be used
  import_url             = each.value.import_url
}



