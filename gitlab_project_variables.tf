resource "gitlab_project_variable" "host" {
  for_each = local.projects

  project   = local.gitlab_project_ids[each.value.name]
  key       = "SC_HOST"
  value     = var.sc_host
  protected = false
}

resource "gitlab_project_variable" "apikey" {
  for_each = local.projects

  project   = local.gitlab_project_ids[each.value.name]
  key       = "SC_API_KEY"
  value     = var.sc_api_key
  protected = false
}

resource "gitlab_project_variable" "project" {
  for_each = local.projects

  project   = local.gitlab_project_ids[each.value.name]
  key       = "SC_PROJECT"
  value     = each.value.name
  protected = false
}

resource "gitlab_project_variable" "statekey" {
  for_each = local.projects

  project   = local.gitlab_project_ids[each.value.name]
  key       = "PROJECT_TF_STATE"
  value     = each.value.name
  protected = false
}



