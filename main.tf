locals {
  projects = {
    for k in fileset("config", "project-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  accounts = {
    for k in fileset("config", "account-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  bsls = {
    for k in fileset("config", "bsl-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  profiles = {
    for k in fileset("config", "profile-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(file("config/${k}"))
  }

  teams = {
    for k in fileset("config", "team-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(templatefile("config/${k}", {}))
  }

  registries = {
    for k in fileset("config", "registry-*.yaml") :
    trimsuffix(k, ".yaml") => yamldecode(templatefile("config/${k}", {}))
  }
}

module "Spectro" {
  source = "github.com/spectrocloud/terraform-spectrocloud-modules"
  #source  = "spectrocloud/modules/spectrocloud"
  #version = "0.0.7"

  projects   = local.projects
  accounts   = local.accounts
  bsls       = local.bsls
  profiles   = local.profiles
  teams      = local.teams
  registries = local.registries
}
