terraform {
  required_version = ">= 0.14.0"

  required_providers {
    spectrocloud = {
      version = "= 0.6.7-pre"
      source  = "spectrocloud/spectrocloud"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.6.0"
    }
  }

#  backend "s3" {
#    bucket = "terraform-state-spectro-rishi"
#    key    = "project-tf-admin/gitlab-terraform.tfstate"
#    region = "us-east-1"
#  }

    backend "http" {
    }
}

variable "sc_host" {}
variable "sc_api_key" {
  sensitive   = true
}

provider "spectrocloud" {
  host         = var.sc_host
  api_key     = var.sc_api_key
  project_name = ""
}

variable "gitlab_token" {}

provider "gitlab" {
  token = var.gitlab_token
}
